package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int cols;
    private CellState[][] states;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.states = new CellState[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                set(row, col, initialState);
            }
        }
    }

    // Updated IGrid
    public boolean outOfBounds(int row, int col) {
        if (row < 0 || col < 0) {
            return true;
        }
        if (row >= numRows() || col >= numColumns()) {
            return true;
        }
        return false;
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (outOfBounds(row, column)) {
            throw new IndexOutOfBoundsException();
        }
        else {
            this.states[row][column] = element;
        }
        
    }

    @Override
    public CellState get(int row, int column) {
        if (outOfBounds(row, column)) {
            throw new IndexOutOfBoundsException();
        }
        else {
            return this.states[row][column];
        }
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(numRows(), numColumns(), CellState.DEAD); 
        // gridCopy.states = this.states;
        for (int row=0; row < numRows(); row++) {
            for (int col=0; col < numColumns(); col++) {
                gridCopy.set(row, col, this.get(row, col));  
            }
        }
        return gridCopy;
    }
    
}
